//! CREATE A DATABASE 
CREATE DATABASE blog_db;


//! Go to specific DATABASE 

USE blog_db;


//! CREATING TABLES FOR DATABASE 


CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(50),
    password VARCHAR(50) NOT NULL,
    datetime_created DATETIME
    PRIMARY key (id)

);


CREATE TABLE posts (
    id INT NOT NULL AUTO_INCREMENT,
    author_id INT NOT NULL,
    title VARCHAR(50),
    content VARCHAR(5000),
    datetime_posted DATETIME,
    PRIMARY key (id)
    CONSTRAINT fk_post_author_id
        FOREIGN KEY (author_id)
        REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT

);

CREATE TABLE reacts (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    post_id INT NOT NULL,
    datetime_reacted DATETIME,
    PRIMARY key (id),
    CONSTRAINT fk_reacts_user_id
        FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_reacts_post_id
        FOREIGN KEY (post_id)
        REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT  
);



CREATE TABLE comments (
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
    content VARCHAR(5000),
    datetime_commented DATETIME,
    PRIMARY key (id),
    CONSTRAINT fk_comments_user_id
        FOREIGN KEY (user_id)
         REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,

    CONSTRAINT fk_comments_post_id
        FOREIGN KEY (post_id)
        REFERENCES posts(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


